# cereal_lib
Based on this problem: https://mste.illinois.edu/reese/cereal/cereal.php. This library runs simulations and returns basic 
statistical results.

Uses rand::thread_rng

Write `cereal_lib = "2.1.1` or `cereal_lib = { git = "https://gitlab.com/notrodes/cereal_lib" }` in your cargo.toml to 
use.
