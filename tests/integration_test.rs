use cereal_lib;

const LOOP_NUMBERS: [u32; 6] = [0, 1, 10, 100, 1_000, 10_000];

#[test]
fn run_multi() {
    println!("Multi-threaded:");
    for number_of_loops in LOOP_NUMBERS.iter() {
        println!("Number of Simulations:{}", number_of_loops);
        let data = cereal_lib::simulation(*number_of_loops, 8);
        println!(
            "Mean: {:?} Median: {:?} Max: {:?} Min: {:?}\n",
            data.mean(),
            data.median(),
            data.max(),
            data.min()
        )
    }
}

#[test]
fn run_single() {
    println!("Multi-threaded:");
    for number_of_loops in LOOP_NUMBERS.iter() {
        println!("Number of Simulations:{}", number_of_loops);
        let data = cereal_lib::simulation_single_thread(*number_of_loops);
        println!(
            "Mean: {:?} Median: {:?} Max: {:?} Min: {:?}\n",
            data.mean(),
            data.median(),
            data.max(),
            data.min()
        )
    }
}
